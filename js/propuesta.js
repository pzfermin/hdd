/*QUE OFRECEMOS*/



var propu = document.querySelector("#propuesta");
propu.addEventListener("click", function(){
var why = document.querySelector("#why");
/*** Preparo el entorno de trabajo***/
why.removeChild(document.querySelector("#container"));

/*** SE CREA LA CLASE ***/
 class propuesta{
     constructor( seccion, contenido, titulo, imagen){
 
         this.etSeccion   = document.createElement("section");
         this.etTitulo  = document.createElement("h3");
         this.etArticulo = document.createElement("article");    
         this.etImagen    =  document.createElement("img");
         this.etDiv = document.createElement("div");
         this.parrafo = document.createElement("p");

         this.etTitulo.innerHTML=titulo;
         this.parrafo.innerHTML=contenido;
         this.etImagen.src=imagen;
         this.etSeccion.appendChild(this.etImagen);
         this.etArticulo.appendChild(this.etTitulo);
         this.etArticulo.appendChild(this.parrafo);
         this.etSeccion.appendChild(this.etArticulo);
         this.etDiv.appendChild(this.etSeccion) 
         
         //ASIGNO CLASES Y ESTILOS
         this.etImagen.classList.add("img-profile", "img-fluid", "img-prop-size");
         this.etArticulo.classList.add("article-margin-side");
         this.etSeccion.classList.add("flex-content-start");
         // this.etSeccion.classList.add("section-propuesta");
         this.etDiv.classList.add("section-propuesta");
         this.etDiv.classList.add("col-md-6");
     }
 }

 /*** Desarrollo del contenido ***/
 let seccion      = ["navegadorAmigable", "diseño", "responsive", "tecnologias"];
 let titulo       = ["Navegador Amigable", "Diseño de primer nivel", "Responsive Design", "Nuevas Tecnologias"];
 let contenido    = [];
     contenido[0] = "Sitios web que funcionan en todos los navegadores, y compatibles con las últimas tecnologías.";
     contenido[1] = "Tu sitio web es la cara visible online de tu marca o empresa, genera confianza y profesionalismo para que así los usuarios que ingresen, no solo sean visitantes sino potenciales clientes."
     contenido[2] = "Nuestros desarrollos son llevados a cabo utilizando las últimas tecnologías, las cuales permiten que tu web se adapte a las diferentes resoluciones, para que así se visualice a la perfección en los distintos dispositivos."
     contenido[3] = "La estructura y diseño de un sitio deben enfocarse al usuario y al contenido. HTML5 y CSS3 proporcionan optimización para los motores de búsqueda, y una apariencia amigable."
 let imagen       = [];
     imagen[0]    = "../public_html/images/svg/navegadoramigable.svg";
     imagen[1]    = "../public_html/images/svg/diseño.svg";
     imagen[2]    = "../public_html/images/svg/responsive.svg";
     imagen[3]    = "../public_html/images/svg/nuevastecnologias.svg";

     
seccionNavegador = new propuesta(seccion[0], contenido[0], titulo[0], imagen[0]);
seccionDiseño = new propuesta(seccion[1], contenido[1], titulo[1], imagen[1]);
seccionResponsive = new propuesta(seccion[2], contenido[2], titulo[2], imagen[2]);
seccionTecnologias = new propuesta(seccion[3], contenido[3], titulo[3], imagen[3]);

var linea = document.createElement("hr");
linea.classList.add("col-md-12");

     why.appendChild(seccionNavegador.etDiv);
     why.appendChild(seccionDiseño.etDiv);
     why.appendChild(seccionResponsive.etDiv);
     why.appendChild(seccionTecnologias.etDiv);
     why.appendChild(linea);
    
     why.classList.add("propuesta-estilos");
      
}
)
